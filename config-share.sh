#!/usr/bin/env bash

# Readonly settings
readonly VERSION="3.1.2"
readonly ASCII_SEPARATOR="---------------"

# Suffixes
readonly BACKUP_SUFFIX=".pre-install-config"

# Keys
readonly GENERAL_SETTINGS_KEY="general_settings"
readonly GENERAL_SNIPPETS_KEY="general_snippets"
readonly PACKAGE_KEY="packages"
readonly EXTENSIONS_KEY="extensions"
readonly SETTINGS_KEY="settings"
readonly SNIPPETS_KEY="snippets"

# Directories
CODE_USER_DIR="$HOME/.config/Code/User/"

# Exec
CODE_EXEC="code"

# Files
CONFIG_FILE="config.json"
SETTINGS_JSON_FILE="$CODE_USER_DIR/settings.json"
SNIPPETS_DIR="$CODE_USER_DIR/snippets"
SNIPPETS_FILE_NAME="install-config.code-snippets"

# Settings
IS_SETTINGS="false"
IS_GENERAL_SETTINGS="false"
SETTINGS_STRATEGY="replace"

# Snippets
IS_SNIPPETS="false"
IS_GENERAL_SNIPPETS="false"

# Verbose
IS_VERBOSE="false"

# Fetched variables from $CONFIG_FILE
PACKAGES=()
THEMES=()

# Created variables on runtime
SELECTED_EXTENSIONS=()
SETTINGS="{}"
SNIPPETS="{}"
ALL=()

#######################################
# Show the usage
# Globals:
#   CONFIG_FILE
#   BACKUP_SUFFIX
#   SNIPPETS_FILE_NAME
#   CONFIG_FILE
#   CODE_USER_DIR
# Arguments:
#   None
#######################################
usage() {
  echo "
Usage: $0 [SETTINGS=*]... [OPTIONS]... [ARGUMENTS]... <packages>
Install provided VSCode config, settings, snippets and themes located in $CONFIG_FILE.
For more informations, go to https://gitlab.com/Zhaith-Izaliel/vscode-config

Author: Ribeyre Virgil.
Licensed: GNU GPLv3 Licence.

ARGUMENTS
  -h, --help                Show this message.
  
      --version             Show version number.

      --install             Install selected packages.

      --list-extensions     List provided package extensions.
      
      --list                List available packages.

OPTIONS
  -v, --verbose             Make the script show more logs during its runtime

      --settings            Allow settings to be added alongside each selected packages.
                            This option backup first the settings.json under the name
                            'settings.json$BACKUP_SUFFIX'
        
      --general-settings    Allow general settings to be installed alongside any package installation.
                            You must pass this with --settings to install both general and 
                            package specific settings.This option backup first the settings.json under the name 
                            'settings.json$BACKUP_SUFFIX'

      --snippets            Add packages specific snippets to the code user's snippets folder in the
                            $SNIPPETS_FILE_NAME file.

      --general-snippets    Add general snippets to the code user's snippets folder in the
                            $SNIPPETS_FILE_NAME file.

SETTINGS
      --config-file=FILE    Set config file (must be a JSON file) to read packages from, default:
                            $CONFIG_FILE

      --code-user-dir=DIR   Set path for code's user directory, default: 
                            $CODE_USER_DIR

      --snippets-file=NAME  Set new snippets file name created by the --snippets and --general-snippets
                            flags, default: $SNIPPETS_FILE_NAME 

      --code-exec=EXEC      Set code exec to run the script with. default: code

      --strategy=STRATEGY   Set settings merging strategy, possible values: 'merge', 'replace', 'force-replace'
                            default: 'replace'  
                            - merge : merge settings with existing one.
                            - replace : replace settings only if provided settings aren't empty
                            - force-replace : replace settings wether they are empty or not
"
  exit 2
}

#######################################
# Repport errors to stderr and echo them
# Globals:
#   None
# Arguments:
#   String: error message
# Outputs:
#   Writes message to stdout and to stderr
#######################################
err() {
  echo "[$(date +'%Y-%m-%d|%H:%M:%S%z')]: $*" >&2
}

#######################################
# Format message to verbose output. If the message passed is $ASCII_SEPARATOR
# the output will be the separator unformatted
# Globals:
#   ASCII_SEPARATOR
# Arguments:
#   String: verbose message
# Outputs:
#   Writes message to stdout
#######################################
verbose() {
  if [ "$IS_VERBOSE" = "true" ]; then
    if [ "$1" = "$ASCII_SEPARATOR" ]; then
      echo "$ASCII_SEPARATOR"
    else
      echo -e "[$(date +'%Y-%m-%d|%H:%M:%S%z')]: $1"
    fi
  fi
}

#######################################
# Check if an executable exists based on which and its exit code.
# Globals:
#   None
# Arguments:
#   String: executable to test
#######################################
does_executable_exists() {
  which $1 &>/dev/null
  local exit_code=$?
  if [ $exit_code != "0" ]; then
    err "$1 is not installed."
    exit 2
  fi
}

#######################################
# Check if the path passed point to an existing file
# Globals:
#   None
# Arguments:
#   String: a path
#######################################
is_file() {
  if [ ! -f "$1" ]; then
    err "$1 is not an existing file."
    exit 2
  fi
}

#######################################
# Check if the path passed point to an existing directory
# Globals:
#   None
# Arguments:
#   String: a path
#######################################
is_dir() {
  if [ ! -d "$1" ]; then
    err "$1 is not an existing directory."
    exit 2
  fi
}

#######################################
# Check if the passed packages names correspond to the fetched packages
# Globals:
#   PACKAGES
#   ASCII_SEPARATOR
# Arguments:
#   (String): an array of packages
# Returns:
#   Exit code 2 if the packages passed contains unknown packages
#######################################
check_packages() {
  local unknown_packages=()
  verbose "Checking packages $*..."
  for i in $@; do
    if [[ ! "${PACKAGES[@]}" =~ "$i" ]] && [ "$i" != "all" ]; then
      unknown_packages+=($i)
    fi
  done

  if [ ! -z $unknown_packages ]; then
    echo -e "Unknown package(s) ${unknown_packages[@]}.\nUse --list to list available packages."
    exit 2
  fi
  verbose "Done."
  verbose $ASCII_SEPARATOR
}

#######################################
# Escape double and single quotes in every array element passed
# Globals:
#   None
# Arguments:
#   (String): an array of string
#######################################
escape_quotes_in_arrays() {
  local -n arr=$1
  for i in ${!arr[@]}; do
    local current=$(echo ${arr[$i]} | sed "s/[\"|']//g")
    arr[$i]=$current
  done
}

#######################################
# Fetched selected settings from the passed package and add them to the SETTINGS global
# Globals:
#   IS_SETTINGS
#   SETTINGS
#   SETTINGS_KEY
#   CONFIG_FILE
# Arguments:
#   string: a package
#######################################
create_selected_settings() {
  if [ "$IS_SETTINGS" = "true" ]; then
    verbose "Importing settings for $1..."
    local current_package_settings=$(jq ".$PACKAGE_KEY.$1.$SETTINGS_KEY" "$CONFIG_FILE")
    SETTINGS=$(echo "$SETTINGS $current_package_settings" | jq -s add)
  fi
}

#######################################
# Fetched selected snippets from the passed package and add them to the SNIPPETS global
# Globals:
#   IS_SNIPPETS
#   SNIPPETS
#   SNIPPETS_KEY
#   CONFIG_FILE
# Arguments:
#   string: a package
#######################################
create_selected_snippets() {
  if [ "$IS_SNIPPETS" = "true" ]; then
    verbose "Importing snippets for $1..."
    local current_package_snippets=$(jq ".$PACKAGE_KEY.$1.$SNIPPETS_KEY" "$CONFIG_FILE")
    SNIPPETS=$(echo "$SNIPPETS $current_package_snippets" | jq -s add)
  fi
}

#######################################
# Create selected packages by fetching their extensions, snippets and settings
# Globals:
#   PACKAGES
#   PACKAGE_KEY
#   EXTENSIONS_KEY
#   CONFIG_FILE
#   ALL
#   ASCII_SEPARATOR
# Arguments:
#   (string): a list of strings corresponding to package
#######################################
create_selected_packages() {
  local arr=$@

  check_packages ${arr[@]}

  if [[ "all" =~ "$@" ]]; then
    arr="${PACKAGES[@]}"
  fi

  for package in ${arr[@]}; do
    local current_package_extensions=($(jq ".$PACKAGE_KEY.${package}.$EXTENSIONS_KEY | @sh" $CONFIG_FILE))
    escape_quotes_in_arrays current_package_extensions

    verbose "Importing package $package..."
    export declare "$package"="${current_package_extensions[*]}"
    ALL+=(${current_package_extensions[*]})

    create_selected_settings $package
    create_selected_snippets $package

    verbose "Done."
    verbose $ASCII_SEPARATOR
  done
}

#######################################
# Initialize PACKAGES with every existing packages in the CONFIG_FILE
# Globals:
#   PACKAGES
#   CONFIG_FILE
#   ASCII_SEPARATOR
# Arguments:
#   None
# Outputs:
#   Exit code 2 if the CONFIG_FILE isn't in valid JSON (validating with jq)
#######################################
initialize() {
  if [ -f "$CONFIG_FILE" ]; then
    verbose "Validating $CONFIG_FILE..."
    if [ -z "$(cat $CONFIG_FILE | jq)" ]; then
      err "Your $CONFIG_FILE file isn't in valid JSON, correct the issues reported above before running this script."
      exit 2
    fi
    verbose "Importing packages list..."
    local json_packages=($(jq ".$PACKAGE_KEY | keys | @sh" $CONFIG_FILE))
    escape_quotes_in_arrays json_packages
    PACKAGES=(${json_packages[*]})
    verbose "Done."
    verbose $ASCII_SEPARATOR
  fi
}

#######################################
# Install extensions with CODE_EXEC
# Globals:
#   CODE_EXEC
# Arguments:
#   (string): list of vscode extensions id
#######################################
install_extensions() {
  for i in $@; do
    $CODE_EXEC --force --install-extension $i
  done
}

#######################################
# Set CONFIG_FILE global and run initialize function
# Globals:
#   CONFIG_FILE
# Arguments:
#   None
#######################################
settings_config_file() {
  is_file "$1"
  CONFIG_FILE="$1"
  initialize
}

#######################################
# Set CODE_USER_DIR global and check SETTINGS_JSON_FILE and SNIPPETS_DIR
# Globals:
#   CONFIG_FILE
#   SNIPPETS_DIR
#   SETTINGS_JSON_FILE
# Arguments:
#   None
#######################################
settings_code_user_dir() {
  is_dir "$1"
  CODE_USER_DIR="$1"
  is_file "$SETTINGS_JSON_FILE"
  is_dir "$SNIPPETS_DIR"
}

#######################################
# Install settings in the settings.json file using provided strategy
# Globals:
#   IS_GENERAL_SETTINGS
#   IS_SETTINGS
#   SETTINGS_STRATEGY
#   BACKUP_SUFFIX
#   SETTINGS_JSON_FILE
#   SETTINGS
#   ASCII_SEPARATOR
# Arguments:
#   None
#######################################
install_settings() {
  if [ "$IS_SETTINGS" = "true" ] || [ "$IS_GENERAL_SETTINGS" = "true" ]; then
    verbose "Creating new settings file with strategy $SETTINGS_STRATEGY..."
    local update_settings="false"

    case "$SETTINGS_STRATEGY" in
    force-replace)
      update_settings="true"
      ;;
    replace)
      if [ "$SETTINGS" != "{}" ]; then
        update_settings="true"
      else
        verbose "Settings are empty, no file will be altered."
      fi
      ;;
    merge)
      local current_settings=$(cat "$SETTINGS_JSON_FILE" | jq)
      if [ -z $current_settings ]; then
        err "Your settings.json file isn't in valid JSON, correct the issues reported above before merging."
      else
        SETTINGS=$(echo "$SETTINGS $current_settings" | jq -s add)
        update_settings="true"
      fi
      ;;
    *)
      err "$SETTINGS_STRATEGY isn't a valid strategy, settings were not updated."
      ;;
    esac

    if [ "$update_settings" = "true" ]; then
      mv "$SETTINGS_JSON_FILE" "${SETTINGS_JSON_FILE}${BACKUP_SUFFIX}"
      echo "$SETTINGS" >"$SETTINGS_JSON_FILE"
      verbose "Done."
    fi

    verbose $ASCII_SEPARATOR
  fi
}

#######################################
# Install settings in the SNIPPETS_FILE_NAME file using provided strategy
# Globals:
#   IS_GENERAL_SNIPPETS
#   IS_SNIPPETS
#   SNIPPETS_DIR
#   SNIPPETS_FILE_NAME
#   SNIPPETS
#   ASCII_SEPARATOR
# Arguments:
#   None
#######################################
install_snippets() {
  if [ "$IS_SNIPPETS" = "true" ] || [ "$IS_GENERAL_SNIPPETS" = "true" ]; then
    verbose "Creating new snippets file..."
    if [ "$SNIPPETS" = "{}" ]; then
      verbose "Snippets are empty, no file will be altered."
    else
      echo "$SNIPPETS" >"$SNIPPETS_DIR/$SNIPPETS_FILE_NAME"
      verbose "Done."
    fi
    verbose $ASCII_SEPARATOR
  fi
}

#######################################
# Install passed packages with settings and snippets
# Globals:
#   CONFIG_FILE
#   ALL
#   ASCII_SEPARATOR
# Arguments:
#   (string): list of packages to install
# Outputs:
#   Exit code 0
#######################################
command_install() {
  is_file $CONFIG_FILE
  create_selected_packages $@

  verbose "Installing selected packages..."
  if [[ "all" =~ "$@" ]]; then
    install_extensions ${ALL[@]}
  else
    for package in $@; do
      local current_package_extensions="${!package}"
      install_extensions "${current_package_extensions[@]}"
    done
  fi
  verbose "Done."
  verbose $ASCII_SEPARATOR

  install_settings
  install_snippets

  exit 0
}

#######################################
# List every extensions provided by the passes packages
# Globals:
#   CONFIG_FILE
#   ALL
# Arguments:
#   (string): list of packages to show extensions from
# Outputs:
#   Exit code 0
#######################################
command_list_extensions() {
  is_file "$CONFIG_FILE"
  create_selected_packages $@

  if [[ "all" =~ "$@" ]]; then
    echo "Extensions in all:"
    for i in "${ALL[@]}"; do
      echo "- $i"
    done
  else
    for package in $@; do
      extensions="${!package}"
      echo "Extensions in $package:"
      for i in "${extensions[@]}"; do
        echo "- $i"
      done
      echo -e "\n"
    done
  fi
  exit 0
}

#######################################
# List every packages name
# Globals:
#   PACKAGES
# Arguments:
#   None
# Outputs:
#   Exit code 0
#######################################
command_list() {
  for i in ${PACKAGES[@]}; do
    echo $i
  done
  exit 0
}

#######################################
# Set IS_GENERAL_SETTINGS to true and fetch them to put them in SETTINGS global
# Globals:
#   SETTINGS
#   IS_GENERAL_SETTINGS
#   GENERAL_SETTINGS_KEY
#   CONFIG_FILE
# Arguments:
#   None
#######################################
option_general_settings() {
  IS_GENERAL_SETTINGS="true"
  local fetched_settings=$(jq ".$GENERAL_SETTINGS_KEY" "$CONFIG_FILE")
  [[ -z $fetched_settings ]] && SETTINGS="{}" || SETTINGS="$fetched_settings"
}

#######################################
# Set IS_GENERAL_SNIPPETS to true and fetch them to put them in SNIPPETS global
# Globals:
#   SNIPPETS
#   IS_GENERAL_SNIPPETS
#   GENERAL_SNIPPETS_KEY
#   CONFIG_FILE
# Arguments:
#   None
#######################################
option_general_snippets() {
  IS_GENERAL_SNIPPETS="true"
  local fetched_snippets=$(jq ".$GENERAL_SNIPPETS_KEY" "$CONFIG_FILE")
  [[ -z $fetched_snippets ]] && SNIPPETS="{}" || SNIPPETS="$fetched_snippets"
}

#######################################
# Run script and parse its argument.
# Globals:
#   ARGS
#   OPTARG
#   OPTERR
#   OPTIND
#   CODE_EXEC
#   SNIPPETS_FILE_NAME
#   SETTINGS_STRATEGY
#   IS_SETTINGS
#   IS_SNIPPETS
#   IS_VERBOSE
#   VERSION
# Arguments:
#   (string): script argument
# Outputs:
#   Exit code 0 if the arguments exists
#   Exit code 2 if the argument doesn't exist
#######################################
main() {
  does_executable_exists jq
  initialize
  while getopts "hv-:" ARGS; do
    case "${ARGS}" in
    -)
      case "${OPTARG}" in
      config-file=*)
        local val=${OPTARG#*=}
        settings_config_file "$val"
        ;;
      code-user-dir=*)
        local val=${OPTARG#*=}
        settings_code_user_dir "$val"
        ;;
      code-exec=*)
        local val=${OPTARG#*=}
        CODE_EXEC="$val"
        ;;
      snippets-file=*)
        local val=${OPTARG#*=}
        SNIPPETS_FILE_NAME="$val"
        ;;
      strategy=*)
        local val=${OPTARG#*=}
        SETTINGS_STRATEGY="$val"
        ;;
      settings)
        IS_SETTINGS="true"
        ;;
      general-settings)
        option_general_settings
        ;;
      snippets)
        IS_SNIPPETS="true"
        ;;
      general-snippets)
        option_general_snippets
        ;;
      verbose)
        IS_VERBOSE="true"
        ;;
      install)
        until [ -z ${!OPTIND} ]; do
          local selected_packages+=("${!OPTIND}")
          OPTIND=$(($OPTIND + 1))
        done
        command_install ${selected_packages[@]}
        ;;
      list-extensions)
        until [ -z ${!OPTIND} ]; do
          local selected_packages+=("${!OPTIND}")
          OPTIND=$(($OPTIND + 1))
        done
        command_list_extensions ${selected_packages[@]}
        ;;
      list)
        command_list
        ;;
      help)
        usage
        ;;
      version)
        echo "$VERSION"
        exit 0
        ;;
      *)
        if [ "$OPTERR" = 1 ] && [ "${optspec:0:1}" != ":" ]; then
          err "Unknown option --${OPTARG}. Use -h or --help for a list of options."
        fi
        exit 2
        ;;
      esac
      ;;
    h)
      usage
      ;;
    v)
      IS_VERBOSE="true"
      ;;
    *)
      exit 2
      ;;
    esac
  done
}

main $*
