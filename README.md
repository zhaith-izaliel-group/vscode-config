![logo.png](img/logo.png)
_Sharing your VSCode config with your colleagues and friends, as simple as just running a script !_

# Important

As of September 9th 2023, **I'm archiving this repository**. Things have changed since I made this util and now I'm fully working on Neovim and not using VSCode anymore. On top of that, I also moved to NixOS so this script was obsolete for me for a long while. If you want to maintain this script, feel free to fork it!

# Config Share

This project is a tiny bash executable that aim to share flexibly a VSCode configuration split up in 3 major components: extensions, settings and snippets.

This script only works with a single json file providing the configuration. This configuration can be split up in packages containing extensions, settings and snippets for a specific scope you decide ! You want to share your configuration with your colleagues but it includes support for many different languages while they only need C#? No more struggling, they just have to install the C# package provided with _a single command_ and that's it, their config works out of the box!

# Author

Ribeyre Virgil

# Table of contents

- [Author](#author)
- [Table of contents](#table-of-contents)
- [Dependencies](#dependencies)
- [Setup instructions](#setup-instructions)
- [Installing my configuration](#installing-my-configuration)
- [Documentation](#documentation)
  - [Basic Usage](#basic-usage)
  - [Using provided settings](#using-provided-settings)
    - [Basic usage](#basic-usage-1)
    - [Merge strategy](#merge-strategy)
      - [Using strategies](#using-strategies)
      - [Caveat regarding the `merge` strategy](#caveat-regarding-the-merge-strategy)
  - [Using provided snippets](#using-provided-snippets)
    - [Basic installation](#basic-installation)
    - [Changing snippets file name](#changing-snippets-file-name)
  - [Using a custom configuration](#using-a-custom-configuration)
  - [Creating your own `config.json` file](#creating-your-own-configjson-file)
  - [The `all` package](#the-all-package)
- [Caveat for Mac users](#caveat-for-mac-users)
- [Contributing](#contributing)
- [Licence](#licence)

# Dependencies

- [jq](https://stedolan.github.io/jq/)
- [Visual Studio Code](https://code.visualstudio.com/) (Classic or Insiders, you can provide your custom executable)

# Setup instructions

- Clone the repository
- Make the script executable by running as root:

```bash
chmod +x ./config-share
```

# Installing my configuration

~~As of the release of the 3.0.0 version of the project, my own config was moved to its [own repository](https://gitlab.com/zhaith-izaliel-group/configuration/zhaith-vscode-config#installation-instruction).~~
~~Follow [this link](https://gitlab.com/zhaith-izaliel-group/configuration/zhaith-vscode-config#installation-instruction) to access my configuration installation instructions.~~

# Documentation

## Basic Usage

This script works by installing packages. Each package has its own set of extensions, settings and snippets corresponding to the provided extensions. These extensions are designed by their ID in the VSCode marketplace. Each package is provided in a `config.json` file and is used before installing any package to fetch and parse them.

To install a package, run:

```bash
./config-share --install package
```

For a full list of possible arguments, run:

```bash
./config-share --help
```

## Using provided settings

### Basic usage

By default, it installs every extensions from a package **without** the provided settings.
To add them alongside the selected packages, you must use the flag `--settings` before installing any package:

```bash
./config-share --settings --install packages
```

The script also provides general settings that don't fit in any specific package settings (like font usage or indent size etc.). You can add them by using the flag `--general-settings` before installing any package:

```bash
./config-share --general-settings --install packages
```

Both flags `--settings` and `--general-settings` can be used as the same time.

**N.B**: _Using the provided settings entirely change your `settings.json` file. Before doing it, **the script will back it up** under the name `settings.json.pre-install-config` in the same directory._

### Merge strategy

#### Using strategies

By default share-config will replace your current `settings.json` file with the new one containing every selected settings. This behavior can be altered by using another **merge strategy**.

Three strategies are currently available:

- `replace`: default strategy, will replace your current `settings.json` file if the provided settings aren't empty.
- `force-replace`: will replace your current `settings.json` file wether or not the provided settings are empty.
- `merge`: will merge your existing settings with the provided ones, with the new settings taking precedence over the old ones.

You can select your current merge strategy by running:

```bash
./config-share.sh --strategy=your-strategy --settings --install packages
```

**N.B**: _These strategies work for both general settings and package specific settings_.

#### Caveat regarding the `merge` strategy

Since the merge strategy uses jq to process your current settings and merge them, they have to be in **valid JSON**. We advise you to check your current `settings.json` file before using this strategy by using tools like [JSONLint](https://jsonlint.com/).

## Using provided snippets

### Basic installation

By default, the script only installs every extensions from a package **without** the provided snippets.
To add them alongside the selected packages, you must use the flag `--snippets` before installing any package:

```bash
./config-share --snippets --install packages
```

The script also provides general snippets that don't fit in any specific package snippets (like copyright snippets for example). You can add them by using the flag `--general-snippets` before installing any package:

```bash
./config-share --general-snippets --install packages
```

Both flags `--snippets` and `--general-snippets` can be used as the same time.

**N.B:** _The script will create a file under your snippets directory named `install-config.code-snippets` by default containing every installed snippets._

### Changing snippets file name

You can change the default name of the created snippets file with the flag `--snippets-file-name`:
(default: `install-config.code-snippets`)

```bash
./config-share --snippets-file="name" --snippets --install packages
```

**N.B**: _This flags **MUST BE USED BEFORE** any other flags or arguments._

## Using a custom configuration

One might want to use a custom configuration according to their needs.

You can set a custom VSCode executable by using the flag `--code-exec=`:
(default: `code`)

```bash
./config-share --code-exec="code-insiders" --install packages
```

It is also possible to change the location of your user code directory with the flag `--code-user-dir=`:
(default: `$HOME/.config/Code/User/`)

```bash
./config-share --code-user-dir="path/to/code-user-dir" --install packages
```

Moreover you can use a custom `config.json` file with `--config-file=`:
(default: `config.json`)

```bash
./config-share --config-file="path/to/config.json" --install packages
```

## Creating your own `config.json` file

The `config.json` file contains information about the packages configuration. It is always structured in the same manner:

```json
{
  "packages": {
    "base": {
      "extensions": [
        "formulahendry.terminal",
        "alefragnani.project-manager",
        "visualstudioexptteam.vscodeintellicode"
      ],
      "settings": {
        "projectManager.any.baseFolders": [
          "/home/zhaith/Documents/Developpement"
        ],
        "projectManager.any.maxDepthRecursion": 2
      },
      "snippets": {
        "Print to console": {
          "scope": "javascript,typescript",
          "prefix": "log",
          "body": ["console.log('$1');", "$2"],
          "description": "Log output to console"
        }
      }
    }
  },
  "general_settings": {
    "editor.formatOnSave": true,
    "editor.formatOnPaste": true,
    "editor.wordWrap": "on"
  },
  "general_snippets": {
    "Print to console": {
      "scope": "javascript,typescript",
      "prefix": "log",
      "body": ["console.log('$1');", "$2"],
      "description": "Log output to console"
    }
  }
}
```

In this example, under the `packages` key you create your packages with the structure:

```json
  "name" : {
    "extensions" : ["ids"], //List of extensions ids, can be empty.
    "settings": {
      // Your settings, can be empty.
    },
    "snippets": {
      // Your snippets, can be empty.
    }
  }
```

Here is a template of a correct `config.json` file:

```json
{
  "packages": {
    "name": {
      "extensions": [
        // List of extensions ids, can be empty.
      ],
      "settings": {
        // Your settings, can be empty.
      },
      "snippets": {
        // Your snippets, can be empty.
      }
    }
  },
  "general_settings": {
    // Your general settings, can be empty.
  },
  "general_snippets": {
    // Your general snippets, can be empty.
  }
}
```

Every settings or snippets are written the same way as VSCode uses them.

**N.B**: _We advise you to check if your `config.json` file is in valid JSON, otherwise the script stops and warns you. You can check if your config.json file is in valid json by using tools like [JSONLint](https://jsonlint.com/)_.

## The `all` package

The `all` package is an alias to install every other listed packages. **You don't have to create it**, it is created programmatically for any provided configuration.

# Caveat for Mac users

This script uses the GNU implementation of `getopts`. I can't guarantee you this will work out of the box for Mac. If you can't run it due to a `getopts` error, please install `gnu-getopt` with [Homebrew](https://brew.sh/).

# Contributing

If you wish to contribute to this project, please, follow the [contributing guidelines](https://gitlab.com/Zhaith-Izaliel/vscode-config/-/blob/master/CONTRIBUTING.md) before opening any issues or merge requests.

# Licence

This script is licensed under the GNU GPLv3 Licence, Copyright 2021, Virgil Ribeyre.
