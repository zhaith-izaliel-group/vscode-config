# CHANGELOG

- [CHANGELOG](#changelog)
    - [Version 3.1.3](#version-313)
    - [Version 3.1.2](#version-312)
    - [Version 3.1.1](#version-311)
  - [Version 3.1.0](#version-310)
  - [Version 3.0.0](#version-300)
    - [Dependencies](#dependencies)
    - [Packages fetching method](#packages-fetching-method)
    - [Installation options](#installation-options)
    - [Settings](#settings)
    - [Miscellaneous](#miscellaneous)
  - [Version 2.1.0](#version-210)
  - [Version 2.0.0](#version-200)
  - [Version 1.3.0](#version-130)
  - [Version 1.2.0](#version-120)
  - [Version 1.1.1](#version-111)
  - [Version 1.1.0](#version-110)
  - [Version 1.0.0](#version-100)

### Version 3.1.3

- Added support for NixOS

### Version 3.1.2

- Add coding style and processes to CONTRIBUTING.md
- Add function documentation on script
- Set styles rules to the script

### Version 3.1.1

- Corrected documentation on flag `--snippets-file`
- Added version number in config-share

## Version 3.1.0

- Added json validation before running the script.
- Added settings merging strategy:
  - `replace`: default strategy, will replace your current `settings.json` file if the provided settings aren't empty.
  - `force-replace`: will replace your current `settings.json` file wether or not the provided settings are empty.
  - `merge`: will merge your existing settings with the provided ones, with the new settings taking precedence over the old ones.

## Version 3.0.0

The project is now named `config-share` and aims to share your own vscode config among your colleagues and friends as easily as just running a script.

### Dependencies

- Uses `jq` to process package name

### Packages fetching method

- Change packages installation and creation: now the script uses a single json file to process packages, themes, and even settings and snippets.

### Installation options

- Add `--settings` to install given settings in settings.json file. It will backup first the old settings.json file under the name `settings.json.pre-install-config`. These settings are fetch depending on the installed packages at first: For example, if you install the `base` package, you will install both its `extensions` and its `settings`
- Add `--general-settings` to install all the settings nested under the `general_settings` key.
- Add `--snippets` to install given snippets in a dedicated file. These snippets are installed depending on the packages installed and thus, fetched only when necessary.
- Add `--general-snippets` flag to install all the snippets nested under the `general_snippets` key.

_See the README.md for more information on installation options._

### Settings

- Add `--code-user-dir=<dir>` to set the VSCode user directory.
- Changed `--packages-file=<dir>` to `--config-file=<file.json>` to get packages from.
- Add support for custom VSCode executable with `--code-exec=<exec>` flag.
- Add support for custom snippets file name with `--snippet-file-name=<name>` flag.

### Miscellaneous

- Change licence to GNU GPLv3 (as MIT conflicted with Bash licensing)
- Add `--verbose` flag to see verbose outputs.
- Changed `-v` flag to shorthand for `--verbose` instead of `--version`

## Version 2.1.0

- Add a support for escaping some extensions in a package: put a `#` or a `!` in first character of the extension to escape it and not install it with the package
- Add support for a custom package directory with `--dir="dir"`. You must put this option before the others or the script will fail
- Add `--list extensions <packages>` to list installable extensions for the given packages
- Change script name for `install-vscode-config`.
- Fix installing multiple packages

## Version 2.0.0

- Changed package creation: now you have to create a file named after the package and list line by line extensions ID to install
- Add support for `-h` argument
- Add Changelog
- Add Licence

## Version 1.3.0

- Added `react` package
- Changed `formatters` package:
  - Removed `svipas.prettier-plus`
  - Added `shunqian.prettier-plus`
  - Added `esbenp.prettier-vscode`

## Version 1.2.0

- Added `angular` package

## Version 1.1.1

- Removed `json-beautifier`

## Version 1.1.0

- Added `formatters` package

## Version 1.0.0

- Initial version
